<?php

class Controller_Dashboard extends Controller
{
	public function before(){
		parent::before();
	}
	public function action_index()
	{


		$view = View::forge('main/layout');
		$view->set_global('service_list', Model_Services::service_list());
		$view->header = View::forge('main/header', array('title'=> Lang::get('site.name'), 'load_googlemap'=>true));
		$view->footer = View::forge('main/footer');

		Asset::css(array(
			'dashboard.css', 'font-awesome.css', 'bootstrap.min.css'
		), array(), 'css_loads');

		Asset::js(array(
			'lib/d3.v2.min.js', 'lib/bootstrap.min.js',
			'config.js', 'app.js', 'dashboard.js'
		), array(), 'js_loads');

		if(Cookie::get('isOkToLookieLoo') == 1){
			$view->body = View::forge('dashboard/index');			
		}else{
			$view->body = View::forge('dashboard/login');
			if(Input::post('username')){
				if(Input::post('username') != "test" OR Input::post('pass') != '1234'){
					$view->body->failed = true;
				}else{
					Cookie::set('isOkToLookieLoo', 1, 60*60*24*14, '/', $_SERVER['HTTP_HOST'], false, true);
					Response::redirect('/dashboard');
				}
			}


		}

		$view->body_class = 'home';

		return $view->render();
	}


	public function action_logout(){
		Cookie::delete('isOkToLookieLoo', '/', $_SERVER['HTTP_HOST'], false, true);
		Response::redirect('/dashboard');
	}

	public function action_404()
	{
		
		$view = View::forge('main/modal');
		$view->title = "404: Not Found";
		$view->meta = View::forge('meta');
		$view->set('body', "<h1>Oops!</h1> We couldn't find that page" ,false);
		

		return $view->render();
	}
}
