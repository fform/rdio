<h1 class="title">Privacy Policy</h1>
										
					<div class="hr"></div>
					
					<p>
	We understand how important the privacy of personal information is to our users and we are committed to protecting your privacy. We urge you to read this Privacy Policy carefully because by using our Website you agree to the collection and use of information as set forth in this Privacy Policy. If you do not agree to this Privacy Policy, please do not use this Website.
</p>
<hr size="2" width="100%" align="center">
<p>
	<strong>Effective December 14, 2011</strong>
</p>
<p>
	This Privacy Policy will tell you what information we collect about you and your use of our website (collectively, &#8220;this Website&#8221;, &#8220;the Website&#8221; or &#8220;our Website&#8221;) and/or our services (&#8220;Services&#8221;). It will explain the ch­­­­oices you have about how your personal information is used and how we protect that information.
</p>
<p>
	As part of this Privacy Policy, we specifically address the following:
</p>
<ul type="disc">
<li>
		<a href="#1">The information we collect</a>
	</li>
<li>
		<a href="#2">How we use your information</a>
	</li>
<li>
		<a href="#3">Your choices about privacy</a>
	</li>
<li>
		<a href="#4">Accessing, reviewing, deleting and changing your information</a>
	</li>
<li>
		<a href="#5">Our security practices and procedures</a>
	</li>
<li>
		<a href="#6">How we protect minors</a>
	</li>
<li>
		<a href="#7">How to contact us</a>
	</li>
<li>
		<a href="#8">Changes to this Privacy Policy</a>
	</li>
</ul>
<p>
	<strong>We urge you to read this Privacy Policy carefully because by using our Website you agree to the collection and use of information as set forth in this Privacy Policy. If you do not agree to this Privacy Policy, please do not use this Website</strong> .
</p>
<p>
	<a name="1"></a> <strong>The Information We Collect</strong>
</p>
<p>
	<em>Personal Information</em><br />
	While you are free to use many of the features on our Website without registering or providing information about yourself, some of the tools and Services we offer may require you to submit &#8220;personally identifiable information&#8221; about yourself. &nbsp;Personally identifiable information is information that you voluntarily provide to us and which can be used to uniquely identify you (such as your e-mail address) and which may also include information you may consider sensitive (such as your gender, your birth date, your ethnicity/ancestry, and/or any personal information). &nbsp;Personally identifiable information may be collected when you register on the Website, or when you create or update your profile, or when you communicate with us via phone, e-mail, or regular mail, or when you otherwise request information from us directly. You are responsible for ensuring the accuracy of all personally identifiable information. Inaccurate information may affect the information or service you receive when using our Website and tools and our ability to provide you the best Services or to contact you.
</p>
<p>
	<em>Non-Personal Information</em><br />
	If you do not use the tools or Services on our Website that require you to provide personally identifiable information, the only information we collect from you will be &#8220;non-personal information&#8221; that we gather as you navigate our Website. Such information includes web pages that you view when you visit and browse our Website and may include your computer&#8217;s internet protocol (IP) addresses, your computer&#8217;s operating system and/or browser type, the identity of your internet service provider, referring/exit pages, date/time stamp, and click-stream data (i.e., a list of pages or URLs visited). In some cases, this non-personal information may be collected automatically and stored in log files to help us analyze trends, administer the site, track users&#8217; movements around the site, and gather demographic information about our user base as a whole. However, this information, when used together with cookies (see &#8220;cookies&#8221; below for more information) or if collected when you are logged-in, may be used to link or associate such information to your personally identifiable information.
</p>
<p>
	<em>Cookies</em><br />
	Cookies are small pieces of information sent to your computer&#8217;s hard drive while you are viewing a website. We use a combination of session cookies (which expire once you close your web browser) and persistent cookies (which stay on your computer until you delete them) to track your navigation and use of our Website and to provide you with a more personal and interactive experience on our Website. For example, cookies allow us to dynamically generate advertising and content on our Website pages or in our emails or newsletters or to determine the popularity of certain content or advertisements on our Website. You have the ability to accept or decline cookies. Most web browsers automatically accept cookies, but you can disable this function by changing the cookie profile on your web browser or by following directions provided in your browser&#8217;s &#8220;help&#8221; file. If you disable your cookies, you may not have access to certain features of our Website. However, you do not have to accept our cookies in order to productively use our Website.
</p>
<p>
	<em>Clear GIFs<br /></em> Clear GIFs are clear electronic images that are used to track your movements on our Website. They help us to determine whether content on our Website is effective. We also use clear GIFs in HTML-based e-mails to let us know which e-mails have been opened and acted upon by recipients (including any persons to which the e-mails have been forwarded). Clear GIFs on our Website collect only a limited set of information including a cookie number, time and date of a page view, and a description of the page on which the clear GIF resides. This information helps us assess the effectiveness of certain communications and marketing campaigns.
</p>
<p>
	<a name="2"></a> <strong>How We Use Your Information</strong>
</p>
<p>
	<em>Personal Information</em><br />
	Except as otherwise stated in this Privacy Policy, we do not sell, trade, or rent your personally identifiable information to third parties. However, we do collect and use personally identifiable information about you to authenticate your use of our Website and services, to provide you with a customized web experience, and to provide you with the services you have requested, including sending electronic newsletters and promotional e-mails to you if you have requested to receive such communications from us, or as otherwise necessary to communicate with you or to send you relevant information and materials. For example, if you elect to receive communications from us, we may periodically send you free newsletters and e-mails that directly promote the use of our Website or services or those of our partners or other third parties businesses that offer goods or services that we believe may be of interest to you. Many users appreciate the offerings they receive from such businesses, however, we always allow you to opt out of having your personally identifiable information shared for direct marketing purposes (see &#8220;Choice/Opt-Out&#8221; below).</p>
<p>	<em>Non-Personal Information</em><br />
	We use non-personal information to track and monitor aggregate usage of our website and for internal analysis, quality control, and to make improvements to our Service. We reserve the right to use and disclose this data to third parties in our discretion, including selected third-party web analytics company to help us use this information to track and analyze aggregate usage volume and patterns regarding our Website. For example, we might inform third parties regarding the number of users of our Website and the activities they conduct while on our site or we might also inform a consumer product company (that may or may not be an advertiser on our site) that &#8220;30% of our users live east of the Mississippi&#8221; or that &#8220;25% of our users have tried their product.&#8221; Depending upon the circumstances, we may or may not charge third parties for this aggregate information.
</p>
<p>
	<em>Third-Party Service Providers</em><br />
	We may share your personally identifiable information with selected third-party contractors, vendors, or suppliers that provide us with technology, e-commerce, or other services as necessary to provide you with access to our Website or Services. Generally, access to personally identifiably information by any such third-parties will be limited to the information reasonably necessary for them to perform their limited functions for us. Where feasible, we will always contractually require that they not use or disclose your personally identifiable information for any other purpose. You expressly consent to the sharing of your personally identifiable information with such third-party service providers as described above.
</p>
<p>
	We use third-party advertising companies to serve ads when you visit our Website. These companies may use non-personal information about your visits to this and other websites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, click here: (<a href="http://networkadvertising.org/consumer/opt_out.asp">http://networkadvertising.org/consumer/opt_out.asp</a>).
</p>
<p>
	<em>Third-Party Websites</em><br />
	Our Website may contain links to numerous other third-party websites and we are not responsible for the privacy practices of these other websites. Once you enter another website (through links found in advertisements, or links referenced within our content, or links placed beside the names or logos of sponsors), whether to purchase a product or just to review content, and/or use the tools and services of such other websites, you must look for and review their privacy statements to understand exactly what information they will collect from or about you and how they will use such information. We will make an effort to make it obvious when you leave our Website and enter a third party website. However, our display of advertisements or links to any other company or website is for your convenience and does not signify our endorsement of such company or website (or the contents of the site). We have no control over, we do not review, and our Privacy Policy does not apply to these other companies, sites, or content, or to any collection of data after you click on an advertisement or link to a third party. However, we may have a business arrangement with some of these third party websites who will pay us commissions or referral fees based on the sale of products or services or traffic generated when you link to such website.
</p>
<p>
	<em>Other Commercial Relationships</em><br />
	We may enter into certain other commercial arrangements to enable our partners to provide our service to their customers and/or to provide you with access to their products and services and we may collect fees for those referrals. For example, we may provide co-branded content and services to certain of our partners or other third-parties. In such case, the co-branded pages that you access through such partner&#8217;s websites may have different registration processes and opportunities for information collection. You must look for and review their privacy statements to understand exactly how they will use information collected about you.
</p>
<p>
	<em>Other Submissions or Disclosures</em><br />
	This Privacy Policy also does not protect you when you send personally identifiable information, other information, feedback, suggestions, content, business ideas, concepts or inventions to us via e-mail, or when you post them, or otherwise submit them to us. If you want to keep such personally identifiable or other information, suggestions, content, business ideas, concepts or inventions, private or proprietary, please do not send them in an e-mail message, post them, or submit them to us. In addition, if you choose to disclose your own personally identifiable information, through other means not associated with us, to friends and/or family members, groups of individuals, third-party service providers, doctors or other professionals, and/or other individuals, we strongly recommend that you make such choices carefully. Such personal information, once released or shared, can be difficult to contain and we will have no responsibility or liability for any consequences that may result because you have released or shared personal information with a third party.
</p>
<p>
	<em>Use of Public Forums</em><br />
	Our website may offer use of message boards, chat rooms and other public forums where users with similar interests can share information or where users can post questions for our experts to answer. Any information (including personally identifiable medical or other information) that you reveal in a chat room, message board, or online discussion is by design open to the public and is not a private, secure service. You should think carefully before disclosing any personally identifiable information in any public forum. What you submit may be seen, disclosed to, or collected by third parties and may be used by others in ways we are unable to control or predict, including to contact you for unauthorized purposes. As with any public forum on any website, this information may also appear in third-party search engines like Google, Yahoo, MSN.
</p>
<p>
	<em>Legal Requirements</em><br />
	We may also release personally identifiable information to third parties: (a) to comply with valid legal requirements such as a law, regulation, search warrant, subpoena, or court order; or (b) in special cases, such as in response to a physical threat to you or others, to protect property, or defend or assert legal rights. In the event we are legally compelled to disclose such information, we will attempt to notify you unless doing so would violate the law or court order. We cooperate with law enforcement agencies in identifying those who may be using our servers or Services for illegal activities and we reserve the right to report any suspected illegal activity to law enforcement individuals or entities for investigation or prosecution.
</p>
<p>
	<em>Sale of the Company or its Assets</em><br />
	In the event that our company or assets are acquired by, or we merge with or into, another company or entity, any personally identifiable information or other information owned or possessed by us will be transferred to the acquirer or successor.
</p>
<p>
	<a name="3"></a> <strong>Your Choices about Privacy</strong>
</p>
<p>
	We offer you choices regarding the collection, use, and sharing of your personally identifiable information. When you register on our Website, we may ask you if you want to receive future communications from us (or certain of our selected partners). You may indicate consent to receipt of these communications by checking the box located on the registration form. Should you decide to stop receiving further communications from us or our selected partners, you will have the opportunity to &#8220;opt-out&#8221; by following the unsubscribe instructions provided in the e-mails you receive. You may also proactively make such choices by contacting us as described below under &#8220;Contact Us.&#8221; Despite your indicated e-mail preferences, however, we may still send you notice of any updates to our Terms of Use or Privacy Statement or for other administrative or non-marketing purposes.
</p>
<p>
	<a name="4"></a> <strong>Accessing, Reviewing, Deleting and Changing Your Information</strong>
</p>
<p>
	If your personal information changes, you may correct or update your account by making the change via your account settings or by sending a request to us via the &#8220;Contact Us&#8221; page on our Website. If you want to delete your personally identifiable information you can also e-mail from the &#8220;Contact Us&#8221; page. We try to answer every e-mail within 48 business hours if possible. Keep in mind, however, that there will be residual information that will remain within our databases, access logs, archives, and other records.
</p>
<p>
	<a name="5"></a> <strong>Our Security Practices and Procedures</strong>
</p>
<p>
	We are committed to protecting the security of your personally identifiable information. As a result, we use a variety of industry-standard security technologies and procedures to help protect such information from unauthorized access, use, or disclosure. For example, we store personally identifiable information behind a computer firewall, which is a barrier designed to prevent outsiders from accessing our servers. We also require you to enter a password to access your Personally Identifiable Information. In addition, we protect your information from unauthorized physical access by storing such information in a controlled facility. Finally, except as provided elsewhere in this Privacy Policy, we limit access to personally identifiable information collected by us to only those persons (including employees and contractors) who have a business need for such access.
</p>
<p>
	Despite all of our efforts to protect your personally identifiable information, there is always some risk that an unauthorized third party may find a way around our security systems or that transmissions of your information over the Internet will be intercepted. In addition, please recognize that protecting your personal information is also your responsibility. We ask you to be responsible for safeguarding your passwords used to access our services. You should not disclose your account information to any third party and should immediately notify us of any unauthorized use of your password.
</p>
<p>
	<a name="6"></a> <strong>How we Protect Minors</strong>
</p>
<p>
	We are committed to protecting the privacy of children. Neither our Website, nor any of our Services, are designed or intended to attract children under the age of 13. We do not collect personally identifiable information from any person we actually know is under the age of 13.
</p>
<p>
	<a name="7"></a> <strong>How to Contact Us</strong>
</p>
<p>
	If you have any questions or concerns regarding this Privacy Policy, or if you wish to have access to or request corrections or updates to personally identifiable information in our records, please send an e-mail to our privacy administrator via the &#8220;Contact US&#8221; page.
</p>
<p>
	<a name="8"></a> <strong>Changes to this Privacy Policy</strong>
</p>
<p>
	We reserve the right to modify or amend this Privacy Policy at any time, so please review it frequently. If we decide to modify or amend this Privacy Policy, such changes will be posted here. We ask that you bookmark and periodically review this page to ensure continuing familiarity with the most current version of our Privacy Policy.</p>