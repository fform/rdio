<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller
{
	public function before(){
		parent::before();
	}

	public function action_notrack(){
		Cookie::set('notrack',1);
		$result = DB::select()->from('product')->execute();
		foreach($result as $product){
			echo "<li><a href='/" . $product['name'] . "'>" . $product['name'] . "</a></li>";
		}
	}

	public function action_index()
	{

		$view = View::forge('main/layout');
		
		$view->header = View::forge('main/header', array('title'=> Lang::get('site.name')));
		$view->footer = View::forge('main/footer');

		Asset::css(array(
			'base.css'
		), array(), 'css_loads');
		
		Asset::js(array(
			'config.js', 'app.js', 'boot.js'
		), array(), 'js_loads');

		$view->body = "Boot it!";
		$view->body_class = 'home';

		return $view->render();
	}


	public function action_legal(){
		$view = View::forge('main/layout');
		
		$view->header = View::forge('main/header', array('title'=> Lang::get('site.name')));
		$view->footer = View::forge('main/footer');

		Asset::js(array(
			'config.js', 'app.js'
		), array(), 'js_loads');

		$view->body = View::forge('legal/home');
		$view->body_class = 'home';

		return $view->render();
	}


	/**
	 * The 404 action for the application.
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		
		$view = View::forge('main/modal');
		$view->title = "404: Not Found";
		$view->meta = View::forge('meta');
		$view->set('body', "<h1>Oops!</h1> We couldn't find that page" ,false);
		

		return $view->render();
	}
}
