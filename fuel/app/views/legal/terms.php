			<h1 class="title">Terms of Use</h1>
										
					<div class="hr"></div>
					
					<p>
	We have as prepared these Terms of Use to help you understand the terms governing your use of our Website.&nbsp; We urge you to read these Terms of Use carefully because by using our Website you agree to be bound by these Terms of Use and all applicable laws and regulations governing the user of our Website.&nbsp; If you do not agree to these Terms of Use, please do not user this Website.
</p>
<p>
	______________________________________________________________________________________
</p>
<p align="center">
<p>
	<strong>Acceptance of Terms of Use</strong>
</p>
<p>
	By using this website located at <a href="http://www.upcustomer.com">www.upcustomer.com</a> or any of the services (&#8220;Services&#8221;) offered via this website (collectively, &#8220;this Website&#8221;, &#8220;the Website&#8221; or &#8220;our Website&#8221;), you agree to be bound by these Terms of Use and all applicable laws and regulations governing the use of this Website.&nbsp; You also represent that you are at least 18 years of age and have the legal right and ability to agree to these Terms of Use and to use this Website in accordance with such terms.&nbsp; If you do not wish to be bound by these Terms of Use or such laws or regulations, or if you are not at least 18 years of age, please do not use this Website!
</p>
<p>
	We may revise and update these Terms of Use at any time by updating this posting.&nbsp; You are required to visit this page from time to time to view our most up-to-date Terms of Use and your continued use of our Website will mean you accept the most current version of our Terms of Use.&nbsp; Please note that these Terms of Use contain disclaimers of warranties and limitations on liability that may be applicable to you.
</p>
<p>
	<strong><u>Your Account</u></strong>
</p>
<p>
	You may be required to create a user account in order to access certain portions of our Website.&nbsp; You represent and warrant that the information you provide to create your account is accurate and complete, and that you will promptly update your account if any of the information changes.&nbsp; When you create an account, you will be prompted to create a username and password.&nbsp; We may refuse to establish an account for you or grant you a username or password that is already in use, impersonates someone else, is or may be illegal, is or may be protected by trademark or other proprietary rights, is vulgar or otherwise offensive, or may cause confusion, as determined by us in our sole discretion. &nbsp;
</p>
<p>
	You are solely responsible for taking all reasonable steps to ensure that no unauthorized person shall have access to your passwords or accounts.&nbsp; It is your sole responsibility to (1) control the dissemination and use of your accounts, usernames, and passwords; (2) authorize, monitor, and control access to and use of your account and password; and (3) promptly inform us of any need to deactivate a password or account or of any unauthorized use of your account.&nbsp; You authorize and grant us and all other persons or entities involved in the operation of our Website the right to transmit, monitor, retrieve, store, and use your information in connection with the operation of the Website, processing your requests and transactions, providing the Services to you, maintaining and improving the Website, and as otherwise set forth in our Privacy Policy.&nbsp; We will employ commercially reasonable technical and organizational safeguards against unauthorized disclosure or access to any of your data, including personally identifiable information, consistent with our Privacy Policy.&nbsp; However, you acknowledge that security safeguards, by their nature, are capable of circumvention and we do not and cannot guarantee that your data, including personally identifiable information, or your communications will not be accessed by unauthorized persons capable of overcoming such safeguards or assume any responsibility or liability for any information you submit, or your or third parties&#8217; use or misuse of our Website, or information transmitted or received using our Website.&nbsp; Additionally, you understand and agree that when using this Website, information (including your data) will be transmitted over a medium that may be beyond our control and jurisdiction.&nbsp; Accordingly, we assume no liability for or relating to the delay, failure, interruption, or corruption of any data or other information transmitted in connection with use of this Website.
</p>
<p>
	<strong><u>Privacy</u></strong>
</p>
<p>
	Your privacy is important to us.&nbsp; All information collected from you in connection with your use of our Website or Services is governed by our Privacy Policy.&nbsp; We reserve the right to update our Privacy Policy and practices from time to time at our sole discretion.&nbsp; For more information, please see our Privacy Policy, which is incorporated into these Terms or Use.</p>
<p>
	<strong><u>Ownership of Website</u></strong>
</p>
<p>
	All information and/or content contained on or accessed from our Website, including (without limitation), editorial content, descriptions, community postings, other text, images, audio or video clips, artwork, animations, sound, hyperlinks, or other references or content contained therein (&#8220;Content&#8221;), as well as all databases, software, code, custom graphics, and button icons, as well as the collection, compilation and assembly thereof, and the overall look and distinctiveness of this Website, are the exclusive property of this Website&#8217;s owner and/or its partners, licensors, or suppliers, and are protected by U.S. and international copyright, trademark and other laws. &nbsp;The reproduction, distribution, transmission, sale, transfer, license, creation of derivative works, modification, public display or posting, public performance, publication, reverse engineering, reverse assembly, or other attempt to discover any source code, or any commercial exploitation of this Website or such Content or intellectual property without our express written permission is expressly prohibited.
</p>
<p>
	We grant to you a non-exclusive, non-transferable, revocable, limited license to use our Website (subject to these Terms of Use) and Content solely for noncommercial personal use (which license only includes the right to view or download a single copy of the Content on this Website); provided that you do not remove or obscure any copyright notice, trademark notice, or other proprietary rights notice displayed on or in conjunction with the Content.&nbsp; You may not use this Website or any such Content in any other manner or for any other purpose without our prior written permission and all rights not expressly granted herein are expressly reserved.&nbsp;&nbsp; If you violate any of these Terms of Use, your permission to use the Website and such Content automatically terminates and you must immediately destroy any copies you have made of any portion thereof.
</p>
<p>
	All trademarks, service marks and logos used on this Website, whether registered or unregistered, are owned by us, and/or our partners, licensors, or suppliers. You may not use or display any trademarks, service marks, or logos used on our Website without our prior written consent or the prior written consent of the rightful owner or owners of such marks or logos.&nbsp;&nbsp;</p>
<p>
	<strong><u>Prohibited Activities</u></strong>
</p>
<p>
	You agree not to use this Website to: (a) violate or encourage the violation of any local, state, national, or international law (including the transmission of technical data exported from the U.S. or the country from which you access our Website or purchase our Services); (b) stalk, harass, or harm another individual; (c) collect, harvest, or store personal data about other users; (d) impersonate any person or entity, or otherwise misrepresent your affiliation with a person or entity or allow another to use your information to access the Website; (e) interfere with or disrupt the Website or networks connected to the Website, or disobey any requirements, procedures, policies, or regulations of this Website or networks connected to the Website; (f) reproduce, duplicate, copy, sell, resell, or exploit for any commercial purpose, any portion of our Website or Content; (g) frame the Website, place pop-up windows over its pages, or otherwise affect the display of its pages; (h) use any high volume, automated, or electronic means (including, without limitation, robots, spiders, scripts, or other automated device) to access the Website or any Content; or (i) interfere with or disrupt the Website or our servers in any other ways.&nbsp;
</p>
<p>
	In addition, you agree not to upload, post, email, or otherwise transmit (a) any material that is unlawful, libelous, defamatory, derogatory, abusive, sexually explicit, threatening, vulgar, obscene, profane, offensive, or otherwise objectionable, as determined by us in our sole discretion; (b) any content that infringes any third-party patent, trademark, trade secret, copyright, or other proprietary rights, or that you do not have a right to transmit under law or under contractual or fiduciary relationships; (c) any unsolicited or unauthorized advertising, promotional materials, junk mail or spam, or any other form of solicitation; or (d) any material that contains software viruses or any other computer code, files, or programs designed to interrupt, destroy, or limit the functionality of any computer software or hardware or telecommunications equipment.&nbsp;
</p>
<p>
	You agree that you are solely responsible for any of the foregoing actions and for the consequences of any such actions.
</p>
<p>
	<strong><u>Advertising, Sponsorship and Promotions Policy</u></strong>
</p>
<p>
	<strong>&nbsp;</strong>
</p>
<p>
	We have sole and absolute discretion with respect to interpretation and enforcement of this Advertising, Sponsorship and Promotions policy and all other issues associated with advertising on our Website, including determining the types of advertising that will be accepted and displayed on the Website. &nbsp;
</p>
<p>
	We reserve the right to reject, cancel, or remove at any time any advertising from the Website for any reason and will provide prompt notice to the advertiser upon rejection, cancellation, or removal of any advertising, together with an explanation following the rejection, cancellation, or removal. We will not accept advertising that we believe to be (i) for illegal products; (ii) offensive material, including material that misrepresents, ridicules, or attacks an individual or group on the basis of color, national origin, ethnicity, religion, gender, sexual orientation, age or disability; (iii) related to alcohol, firearms, ammunition, fireworks, gambling, pornography or tobacco; (iv) fraudulent, misleading or deceptive; or (v) injures our good name or reputation or our Website or is otherwise inconsistent with our principles or our Website.
</p>
<p>
	We also recognize the importance of maintaining a distinct separation between advertising content and editorial and decision-making content. We strive to clearly and unambiguously identify all advertising as such. We also reserve the right to determine the appropriate placement of the advertising on the Website and the way in which any and all search results for specific information by keyword or topic are displayed on the Website.&nbsp; For example, we may have arrangements with certain sponsors or affiliates that involve the payment of fees or commissions for click-throughs or purchases.&nbsp; In such cases, such sponsors or affiliates may appear higher in search rankings or even in place of non-partners.&nbsp; In addition, sponsored content must be labeled as being sponsored and the name of the sponsor will be clearly displayed.&nbsp; We will do what it is practically necessary to ensure that you will not confuse sponsored content with content that is original.&nbsp; However, we do not exercise any editorial control over sponsored content and each sponsor is responsible for the accuracy and objectivity of sponsored content. &nbsp;The sponsor&#8217;s content may also contain links to the sponsor&#8217;s web site.&nbsp;&nbsp; You should know that we do not endorse any specific product, service, or treatment mentioned in any sponsored content.
</p>
<p>
	All advertisers and sponsors must comply with all applicable foreign, domestic and local governmental laws, including the regulations of regulatory agencies.
</p>
<p>
	<strong><u>Third-Party Services</u></strong>
</p>
<p>
	This Website may provide links to other sites and resources on the Internet.&nbsp; In addition, we may display ads on our Website that also link to third parties.&nbsp; No such Links to or from our Website constitute any endorsement or recommendation by us of any third-party or its website, or of any specific products, services, advertisements, opinions, resources, or other information, and you acknowledge you are linking to these websites at your own risk. &nbsp;We do not have any control over and do not assume any responsibility or liability for, these outside websites or communications, materials, products or services available at such websites.&nbsp; You are solely responsible for complying with all terms and conditions of use for third-party websites and content and you understand you are subject to the privacy policies of any such websites. &nbsp;You acknowledge that we shall have no liability, directly or indirectly, for any damage or loss arising from your access to, use of, or reliance on any third-party website, communications, materials, products or services.
</p>
<p>
	Should you decide to use the products or services of a third-party provider accessed or referred via our Website, any transaction between you and such third-party will be strictly between you and such party and shall be subject to such third-party&#8217;s specific pricing and specific terms and conditions, regardless of any pricing, terms, or other information referenced on our Website. Pricing information may also contain typographical errors or other errors or inaccuracies and may not be complete or current. &nbsp;We reserve the right to correct any such errors, inaccuracies or omissions and to change or update information at any time without prior notice. &nbsp;However, we do not guarantee that any errors, inaccuracies or omissions will be corrected. &nbsp;&nbsp;&nbsp;&nbsp;
</p>
<p>
	All third-party providers accessed from our Website reserve the right to refuse to fill any orders or provide services based on inaccurate or erroneous information on our Website or for any other lawful reason.&nbsp; In addition, we have no control over such provider&#8217;s performance of its products or services, and we make no representations or warranties with respect to the performance of such services. YOU AGREE THAT WE WILL NOT BE RESPONSIBLE OR LIABLE FOR ANY LOSS OR DAMAGE OF ANY SORT INCURRED AS THE RESULT OF ANY SUCH TRANSACTION WITH ANY SUCH THIRD-PARTY PROVIDERS.
</p>
<p>
	<strong><u>Community Forums</u></strong>
</p>
<p>
	If you use any public area of our Website, such as a chat room, message boards, blogs, or other community tools or forums, you are solely responsible for your own communications, the consequences of posting those communications, and your reliance on any communications you might encounter. You understand and agree that we are not responsible for the consequences of any communications in these areas of the Website and that if you feel threatened or believe someone else is in danger, you should contact your local law enforcement agency immediately.&nbsp; You agree that you must evaluate and bear all risks associated with the use of any Content found in the public areas of the site.&nbsp; You also understand that we do not endorse any opinions expressed by users of the Website and you acknowledge that you will ­not rely on the accuracy, truthfulness, reliability, completeness, or usefulness of any such Content.&nbsp; Any such reliance is at your own risk.
</p>
<p>
	In consideration of being allowed to use our community forums and other public areas of the Website, you must comply fully with these Terms of Use, including, without limitation, the section above titled &#8220;Prohibited Activities.&#8221;&nbsp; In addition, we reserve the right (but we are not obligated) to (a) pre-screen, monitor, review, record, filter, edit, disclose, or delete any dialogue and/or Content posted in public areas in our sole discretion, (b) investigate an allegation that any communication, posting, or action does not conform to these Terms of Use and to determine in our sole discretion to remove or request the removal of the communication, posting, or the user responsible for such communication, posting, or action, (c) terminate a user&#8217;s access to the Website or any public areas of the Website in our sole discretion, (d) disclose any Content, including personally identifiable information, to law enforcement agencies or others if required to do so by law or in the good faith belief that such preservation or disclosure is reasonably necessary to comply with legal process or other obligations that we may owe pursuant to ethical and other professional rules, laws, and regulations, to enforce these Terms of Use, to respond to claims that any content violates the rights of third-parties, or to protect our rights, property, or personal safety of our employees, agents, partners, suppliers, users, clients, and the public.&nbsp; You agree that we shall have no liability or responsibility to users of this Website or any other person or entity for performance or nonperformance of the aforementioned activities.
</p>
<p>
	By submitting content or making postings to the Website, you represent and warrant that you own or otherwise control the rights to such submissions and you grant us a royalty-free, perpetual, irrevocable, non-exclusive, worldwide right and license (with the right to sublicense through multiple tiers) to (a) use, reproduce, distribute, modify, create derivative works of, publicly display, and publicly perform your submissions in any format or media now known or not currently known; and (b) make, have made, sell, offer for sale, import, and otherwise transfer your submissions for commercial or non-commercial purposes. Subject to the foregoing license, you retain all ownership rights in and to your submissions; provided, however, that such submissions (regardless of whether such submissions involve personally identifiable information) will be considered non-confidential and non-proprietary and we will have no obligations with respect to such submissions.&nbsp; In addition, we may post on the Website any testimonial you submit to us.
</p>
<p>
	&nbsp;<br />
	You acknowledge that we may establish general practices and limits concerning use of our Website, including (without limitation) the maximum number of days that content will be retained by us, the maximum disk space that will be allotted on our servers on your behalf, and the maximum number of times (and the maximum duration for which) you may access the Website in a given period of time.&nbsp; You agree that we have no responsibility or liability for the deletion or failure to store any messages and other communications or other content maintained or transmitted on the Website.&nbsp; You further acknowledge that we reserve the right to change the forgoing general practices and limits at any time, in its sole discretion.
</p>
<p>
	<strong><u>Copyright Procedures</u></strong>
</p>
<p>
	Our ­­­­agent for copyright issues relating to the Website is:
</p>
<p>
	[To come]
</p>
<p>
	Attn: CEO
</p>
<p>
	If you believe any materials accessible on or from this Website infringe your copyright, you may request that such content be removed from the Website by contacting our copyright agent and providing the following information:
</p>
<ol start="1" type="1">
<li>Identification of the copyrighted work that you believe to be infringed. Please describe the work, and where possible include a copy or the location (e.g., URL) of an authorized version of the work.
	</li>
<li>Identification of the material that you believe to be infringing and its location. Please describe the material, and provide us with its URL or any other pertinent information that will allow us to locate the material.
	</li>
<li>Your name, address, telephone number and (if available) e-mail address.
	</li>
<li>A statement that you have a good faith belief that the disputed use of the materials is not authorized by the copyright owner, its agent, or the law.
	</li>
<li>A statement by you, made under penalty of perjury, that the information that you have supplied is accurate and that you are the copyright owner or are authorized to act on the copyright owner&#8217;s behalf; and
	</li>
<li>A signature or the electronic equivalent from the copyright holder or authorized representative.
	</li>
</ol>
<p>
	<strong><u>Indemnification</u></strong>
</p>
<p>
	You agree to indemnify, defend and hold us and our partners, licensors, or suppliers, and our and their respective directors, officers, employees, partners, consultants, agents, affiliates, successors and assigns, harmless from and against any and all claims, damages, losses, liabilities, causes of action, suits, proceedings, demands, costs (including reasonable attorneys&#8217; fees) and other expenses (collectively, &#8220;Claims&#8221;) that are related to or arise from (a) your use of our Website or Content, (b) any decisions or recommendations you make based on our Website or Content, (c) your breach or violation of these Terms of Use or of any rights of any other person or entity, (d) any data or communications you submit, post, or transmit to, or through, the Website, or (e) any viruses, trojan horses, worms, time bombs, cancelbots, spyware, or other similar harmful or deleterious programming routines input by you into our Website.&nbsp; You agree to cooperate as reasonably required in the defense of any such Claim.&nbsp; We reserve the right to assume the exclusive defense and control of any matter otherwise subject to indemnification under this paragraph and, in any event, you agree not to settle any such matter without our prior written consent.
</p>
<p>
	<strong><u>Dispute Resolution</u></strong>
</p>
<p>
	These Terms of Use and the relationship between you and us shall be governed by the laws of the State of California, without giving effect to any choice of laws or principles that would require the application of the laws of a different country or state.&nbsp;
</p>
<p>
	Any claim, dispute, or controversy arising out of these Terms of Use, our Website, our Services, or the Content shall be resolved by final and binding arbitration before a single arbitrator (&#8220;Arbitrator&#8221;) selected from and administered by the American Arbitration Association in accordance with its applicable rules. &nbsp;The arbitration hearing shall be held in Los Angeles, California and the prevailing party shall be entitled to reimbursement from the other party of its reasonable attorney&#8217;s fees, costs, and disbursements arising out of the arbitration. Notwithstanding the foregoing, either party may seek interim or preliminary injunctive relief from a court of competent jurisdiction in Los Angeles, California as necessary to protect it rights or property pending the completion of any arbitration proceeding.</p>
<p>
	<strong><u>Disclaimer of Warranties</u></strong>
</p>
<p>
	WE MAKE NO EXPRESS OR IMPLIED WARRANTIES WHATSOEVER WITH RESPOECT TO THIS WEBSITE OR CONTENT AND NOTHING CONTAINED IN THESE TERMS OF USE OR IN OUR WEBSITE OR CONTENT, NOR ANY OTHER STATEMENTS MADE BY ANY OF OUR EMPLOYEES, AGENTS, PARTNERS, OR SUPPLIERS, WHETHER ORAL OR WRITTEN, SHALL CREATE ANY SUCH WARRANTY.&nbsp; YOU EXPRESSLY AGREE THAT THIS WEBSITE AND CONTENT ARE PROVIDED ON AN &#8220;AS IS&#8221; AND &#8220;AS AVAILABLE&#8221; BASIS, AND YOUR USE OF THEM IS AT YOUR OWN RISK. &nbsp;WE EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING (WITHOUT LIMITATION) ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT, OR THAT THIS WEBSITE OR THE CONTENT WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, OR THAT ACCESS TO ANY OF THEM WILL BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE, OR THAT DEFECTS, IF ANY, WILL BE CORRECTED, OR AS TO THE ACCURACY, COMPLETENESS, QUALITY, TIMELINESS, RELEVANCE, OR RELIABILITY OF SUCH WEBSITE OR CONTENT, OR THAT THE SERVERS ON WHICH THIS WEBSITE IS HOSTED ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.&nbsp;
</p>
<p>
	YOU UNDERSTAND AND AGREE THAT WE ASSUME NO RESPONSIBILITY FOR ANY CONSEQUENCE RELATING DIRECTLY OR INDIRECTLY TO ANY ACTION OR INACTION YOU TAKE BASED ON THIS WEBSITE ­­OR ITS CONTENT AND THAT ANY CONTENT DOWNLOADED OR OTHERWISE OBTAINED, ACCESSED, OR RECEIVED FROM US IS DONE SO AT YOUR OWN DISCRETION AND RISK AND YOU MUST EVALUATE AND BEAR ALL RISKS ASSOCIATED WITH ITS USE, INCLUDING ANY RELIANCE ON ITS ACCURACY, COMPLETENESS OR USEFULNESS.&nbsp; YOU ALSO ACKNOWLEDGE AND AGREE THAT ANY PRODUCTS OR SERVICES MADE AVAILABLE ON, BY OR THROUGH OUR WEBSITE ARE SUPPLIED BY THIRD PARTIES THAT WE DO NOT CONTROL. ALTHOUGH WE MAY FACILITATE YOUR ACCESS TO SUCH PRODUCTS OR SERVICES, NO SUCH FACILITATOIN SHALL CREATE ANY LIABILITY ON OUR PART (WHETHER CAUSED BY THE MISCONDUCT OR NEGLIGENCE OF SUCH THIRD PARTY, OR OTHERWISE) AND YOU AGREE, TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, THAT WE SHALL HAVE NO LIABILITY WITH RESPECT TO ANY SUCH PRODUCTS OR SERVICES.
</p>
<p>
	<strong><u>Limitations of Liabilities</u></strong>
</p>
<p>
	TO THE FULLEST EXTENT PERMITTED UNDER APPLICABLE LAW, IN NO EVENT WILL WE OR ANY OF OUR OFFICERS, EMPLOYEES, DIRECTORS, AFFILIATES, PARTNERS, AGENTS, ADVISORS, SUPPLIERS, OR LICENSORS BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES, INCLUDING (BUT NOT LIMITED TO) DAMAGES FOR LOSS OF REVENUES, LOSS OF PROFITS, LOSS OF USE, LOSS OF DATA, COST OF PROCUREMENT OF SUBSTITUTE PRODUCTS OR SERVICES, PERSONAL INJURY, INCLUDING DEATH, OR ANY SUCH OTHER DAMAGES OR INTANGIBLE LOSSES (EVEN IF SUCH PARTIES WERE ADVISED OF, KNEW OF OR SHOULD HAVE KNOWN OF THE POSSIBILITY OF SUCH DAMAGES, AND NOTWITHSTANDING THE FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY), REGARDLESS OF WHETHER SUCH DAMAGES ARE BASED ON CONTRACT, TORT, WARRANTY, STATUTE OR ANY OTHER THEORY OF LIABILITY, IN EACH CASE ARISING OUT OF OR RELATED TO (A) YOUR USE (OR THE INABILITY TO USE) OF THIS WEBSITE OR CONTENT, (B) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR DATA, INCLUDING PERSONALLY IDENTIFIABLE INFORMATION, OR YOUR COMMUNICATIONS; (C) THE STATEMENTS OR CONDUCT OF YOU OR ANY OTHER USER ON THIS WEBSITE; (D) ANY ACTION YOU TAKE BASED ON THE INFORMATION YOU RECEIVE FROM THIS WEBSITE OR CONTENT, (E) YOUR FAILURE TO KEEP YOUR PASSWORD OR ACCOUNT DETAILS SECURE AND CONFIDENTIAL, (F) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; OR (G) ANY OTHER MATTER RELATING TO THIS WEBSITE OR CONTENT.&nbsp; THE AGGREGATE LIABILITY FOR ALL CLAIMS ARISING FROM OR RELATED TO THIS WEBSITE OR CONTENT IS LIMITED TO FIFTY DOLLARS ($50).&nbsp; IF YOU ARE DISSATISFIED WITH ANY PORTION OF THIS WEBSITE OR CONTENT, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USE OF THIS WEBSITE.&nbsp;</p>
<p>	Some jurisdictions do not allow the exclusion of certain warranties or the limitation or exclusion of liability for incidental or consequential damages. &nbsp;Accordingly, some of the above limitations and disclaimers may not apply to you.&nbsp; To the extent that we may not, as a matter of applicable law, disclaim any implied warranty or limit our liabilities, the scope and duration of such warranty and the extent of our liability shall be the minimum permitted under such applicable law.&nbsp; In addition, you may also have other rights under applicable law, which vary from state to state. &nbsp;In the event that applicable law imposes implied warranties notwithstanding the foregoing, such implied warranties shall not have a duration greater than one year from the relevant purchase or first access date (whichever comes first), shall terminate automatically at the end of such period, and shall be disclaimed and excluded to the fullest extent permitted by law.
</p>
<p>
	<strong><u>Modifications to Website or Service</u></strong>
</p>
<p>
	<br />
	We reserve the right to modify, discontinue, or suspend operation of this Website temporarily or permanently, with or without notice to you. &nbsp;You agree that we shall not be liable to you or any third-party should we exercise such right.&nbsp; If you object to any change to our Website, your sole recourse will be to discontinue using the Website. &nbsp;Your continued use of any modified service on the Website will indicate your assent to such changes.
</p>
<p>
	<strong><u>Termination</u></strong>
</p>
<p>
	The agreements established by these Terms of Use shall remain effective until terminated in accordance with these Terms of Use.&nbsp; Either party may terminate these Terms of Use immediately upon notice to the other party.&nbsp; In addition, we may immediately terminate such agreements and/or your use of this Website or any of our Services without prior notice for any reason, in our sole discretion, including (without limitation) if we believe that you have violated these Terms of Use or have acted in manner which shows you do not intend to, or are unable to comply with, these Terms of Use, or if we are required to do so by law (for example, where the provision of this Website to you is, or becomes, unlawful).&nbsp; Upon such termination of by either party, your right to use this Website shall immediately cease, and you shall destroy all copies of information that you have obtained from us.&nbsp; You agree that we shall not be liable to you or any third-party should we exercise such right.&nbsp; You may discontinue your access to the Website at any time.&nbsp; If you wish to terminate your account, you may do so by notifying us at any time in writing.&nbsp; These Terms of Use, including (without limitation) all disclaimers, all limitations of liability, and all our rights of ownership, shall survive any expiration or termination (by either us or you) of use of our Website or these Terms of Use.
</p>
<p>
	<strong><u>Children</u></strong>
</p>
<p>
	This Website is intended for adults in the United States over the age of 18. This Website is not intended or designed to attract children under the age of 13 and we do not collect personally identifiable information from any person we actually know is a child under the age of 13.
</p>
<p>
	<strong><u>General</u></strong>
</p>
<p>
	We are a privately held and privately funded corporation.&nbsp; This Website is maintained by the company from its offices within the United States and we make no representation that materials in the Website are appropriate or available for use in other locations outside of the United States.&nbsp; Those who choose to access this Website from such other locations are solely responsible for compliance with any and all local or national laws, as applicable.&nbsp; You should not construe anything on this Website as a promotion or solicitation for any product or service or use of any product or service that is not authorized by the laws and regulations of the place where you are located.&nbsp;&nbsp;The software, technology and other information available from this Website are further subject to United States export controls and, potentially, the import laws of your jurisdiction.&nbsp; No software, information or content from this Website may be downloaded, exported or re-exported, and no product or service purchased through this Website may be exported or re-exported, into (or to a national or resident of) any countries that are subject to U.S. export restrictions.
</p>
<p>
	These Terms or Use constitute the entire and exclusive and final statement of the agreement between you and us with respect to the subject matter hereof, and govern your use of this Website, superseding any prior agreements or negotiations between you and us with respect to the subject matter hereof.&nbsp; If any provision of these Terms of Use is found to be invalid by any court having competent jurisdiction or the Arbitrator, the invalidity of such provision shall not affect the validity of the remaining provisions of these Terms of Use, which shall remain in full force and effect. &nbsp;No waiver of any of these Terms of Use shall be deemed a further or continuing waiver of such term or condition or any other term or condition.&nbsp; You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to the use of our Website, the Content, or these Terms of Use, must be filed within one (1) year after such claim or cause of action arose or be forever barred. &nbsp;A printed version of this agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. &nbsp;The section titles in these Terms of Use are for convenience only and have no legal or contractual effect. &nbsp;Neither these Terms of Use, nor the agreements created by these Terms of Use, may be assigned, transferred, delegated, or sublicensed by you except with our prior written consent, and any attempted assignment, transfer, delegation, or sublicense shall be null and void.&nbsp; We may assign, transfer, or delegate these Terms of Use and the agreements created hereby in our sole discretion.
</p>
<p>
	<strong><u>Last Updated:&nbsp; December 12, 2011</u></strong></p>