<html>
<head>
	<title><?= $title; ?></title>
	<?= $meta; ?>
	<?= Asset::css("bootstrap.min.css"); ?> 
	<?= Asset::render('css_loads'); ?>
	<?= Asset::render('js_loads'); ?>
	<?= View::forge('ga', array(), false); ?>

</head>
<body class="<?=(isset($body_class)?$body_class:'');?>">
<div id="wrap">
<?= $body; ?>
</body>
</html>