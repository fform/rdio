<?php


class Controller_Api extends Controller_Rest
{
	protected $format = 'json';

	public function before(){
		parent::before();
		//$this->response->set_header('Access-Control-Allow-Origin', '*');
		//$this->response->set_header('Access-Control-Allow-Request-Method', '*');
	}


	public function get_track(){
		Model_History::record('track', Input::get('action'), Cookie::get('uid'));
		return $this->response(array('success'=>true));
	}
}
