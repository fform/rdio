window.Router = {};
window.Model = {};
window.Collection = {};
window.View = {};


window.clog = function(){
	if( AG['debugging'] ){
		console.log(arguments);
	}
}

App = (function(){
	var Application = {

		container: "#container",

		touch: ('ontouchstart' in window),

		init: function () {
			App.benchmark('app.init');
			
			App.track( "benchmark", "app", "init", App.benchmark( 'app.init' ) );
			
			if($.cookie('uid') === null){
				$.cookie('uid', makeUid(11));
			}
			$("#reset").on('click',function(){
				App.reset_auction();
				document.location = document.location;
			});
		},

		reset_auction: function(){
			$.removeCookie('startTime');
			$.removeCookie('product');
			$.removeCookie('lastWatchers');
			$.removeCookie('auctionKilled');
			$.removeCookie('iBoughtIt');

			$.cookie('session', makeUid(6));
		},

		confirm: function( message, buttonLabels, callback, title ){
			callback(confirm(message));
		},

		alert: function( message, buttonName, callback, title ){
			alert(message);
		},

		back_track: function(category,action){
			$.get('api/track',{'action':category + '.' + action});
		},
		
		track: function(category, action, label, value) {

			

			if(AG['tracking'] && App._isTrackingGoogleAnalytics){
				clog([category,action,label,value]);
				var a = [category, action];

				if(label){
					a.push(label);
				}
				if(value !== undefined){
					a.push(value);
				}

				if(action == "view"){
					a.unshift('_trackPageview');
				}else{
					a.unshift('_trackEvent');
				}

				_gaq.push(a);
			}
		},


		benchmark: function( key, clear_start ){
			var now = new Date();
			if(!window._BENCHMARKS){
				window._BENCHMARKS = {};
			}

			if( clear_start === true ){
				delete _BENCHMARKS[key];
			}
			
			if( _BENCHMARKS[key] ){
				var elapsed = now.getTime() - _BENCHMARKS[key];
				return elapsed;
			}else{
				_BENCHMARKS[key] = new Date().getTime();
				return 0;
			}
		}


	};
		return Application;
})();
