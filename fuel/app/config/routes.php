<?php
return array(
	'_root_'  => 'welcome/index',  // The default route
	'_404_'   => 'welcome/404',    // The main 404 route
	'legal'=>'welcome/legal',
	'api/(:any)'=>'api/$1',
	'notrack'=>'welcome/notrack',
	'(:any)' => 'welcome/product/$1'
);