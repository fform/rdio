<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html> 
<head> 
<title><?= $title; ?></title>
<?= View::forge('meta', array(), false); ?>
<?= Asset::css("bootstrap.min.css"); ?> 
<?= Asset::render('css_loads'); ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="/assets/js/lib/jquery.scrollto.min.js"></script>
<script src="/assets/js/lib/jquery.cookie.js"></script>
<script src="/assets/js/lib/number.js"></script>
<script src="/assets/js/lib/bootstrap.min.js"></script>

<?= Asset::render('js_loads'); ?>
<?= View::forge('ga', array(), false); ?>


</head> 
<body class="<?=(isset($body_class)?$body_class:'');?>">
<div id="header-base"></div>
<div class='container'>
<div id="header-space"></div>