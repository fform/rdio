<?php


class Model_History
{
	protected static $_table_name = 'history';

	static public function record($key, $value, $index = 0){
		DB::insert(self::$_table_name)->set(array(
			'key'=> $key,
			'value'=> ((is_int($value) OR is_numeric($value) OR is_bool($value) OR is_string($value)) ? $value : json_encode($value) ),
			'index'=>$index,
			'ip'=>$_SERVER['REMOTE_ADDR'],
			'session'=>(Cookie::get('session')?:""),
			'notrack'=>(Cookie::get('notrack')?:0)
		))->execute();
		
	}
}